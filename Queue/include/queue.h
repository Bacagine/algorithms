/* Github: https://github.com/Bacagine/Algorithms/Queue
 * 
 * queue.h: Functions and variables for the Queue Algorithm
 * 
 * Development by Gustavo Bacagine <gustavo.bacagine@protonmail.com>
 * 
 * Date: 30/12/2020
 */

#ifndef _QUEUE_H
#define _QUEUE_H

#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

#define QUEUE_EMPTY "Queue empty!"
#define QUEUE_FULL  "Queue full!"

static int actual = 0;

bool is_empty(void);
bool is_full(int queue[]);
void enqueue(int queue[], const int element);
int dequeue(int queue[]);
int show_queue(int queue[]);

#endif
