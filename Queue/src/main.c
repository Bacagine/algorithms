/* Github: https://github.com/Bacagine/Algorithms/Queue
 * 
 * main.c: Implemanting the Queue Algorithm
 * 
 * Development by Gustavo Bacagine <gustavo.bacagine@protonmail.com>
 * 
 * Begin's date: 30/12/2020
 * Date of last modification: 31/12/2020 
 */

#include <stdio.h>
#include "../include/queue.h"

int main(void){
    int length = 0;
    
    printf("Please, write the queue length: ");
    scanf("%d", &length);
    
    int *queue = (int *) malloc(length * sizeof(int));
    
    if(show_queue(queue) == 1){
        puts(QUEUE_EMPTY);
    }
    
    srand(time(NULL));
    for(int i = 0; i < length; i++){
        push(queue, rand() % 10);
    }
    
    free(queue);
    queue = NULL;
    
    return 0;
}
