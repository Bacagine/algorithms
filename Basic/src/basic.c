#include <stdio.h>
#include "../include/basic.h"

void show_array(int arr[], int length){
    int i;
    for(i = 0; i < length-1; i++){
        printf("%d ", arr[i]);
    }
    printf("%d\n", arr[i]);
}

void generate_array(int arr[], int length){
    int i;
    srand(time(NULL));
    for(i = 0; i < length; i++){
        arr[i] = rand() % 10;
    }
}

void show_matrix(int i, int j, int mat[i][j]){
    
}

void generate_matrix(int i, int j, int mat[i][j]){
    
}

void print_str(const char *str){
    int i;
    for(i = 0; str[i] != '\0'; i++){
        putchar(str[i]);
    }
}

char *get_str(char *str){
    int i;
    char *aux = NULL;
    for(i = 0; str[i] != '\0'; i++){
        aux[i] = str[i];
    }
    strcpy(str, aux);
    return str;
}

void clear_buffer(void){
    int ch;
    while((ch = getchar()) != '\n' && ch != EOF);
}
