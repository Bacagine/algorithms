/* Github: https://www.github.com/Bacagine/
 * 
 * arr_test.c:
 * 
 * Development by Gustavo Bacagine <gustavo.bacagine@protonmail.com>
 * 
 * Date: 31/12/2020 
 */

#include <stdio.h>
#include "../include/basic.h"

int main(void){
    int *arr = (int *) malloc(length * sizeof(int));
    
    generate_array(arr, length);
    show_array(arr, length);
    
    free(arr);
    arr = NULL;
    
    return 0;
}
