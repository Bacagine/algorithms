#ifndef _BASIC_H
#define _BASIC_H

#include <stdlib.h>
#include <string.h>
#include <time.h>

static const int length = 5;

/* Arrays */
void generate_array(int arr[], int length);
void show_array(int arr[], int length);

/* Matrix */
void generate_matrix(int i, int j, int mat[i][j]);
void show_matrix(int i, int j, int mat[i][j]);

/* Strings */
void print_str(const char *str);
char *get_str(char *str);
void clear_buffer(void);

#endif
